export interface Baller {

    name: string
    club: string
    height: number
    weight: number
    age: number
    value: number
    wage: number
}