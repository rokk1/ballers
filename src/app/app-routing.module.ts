import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BallerDetailsComponent } from './components/baller-details/baller-details.component';
import { BallersListComponent } from './components/ballers-list/ballers-list.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  {
    path: 'home', component: HomeComponent
  }
  ,
  {
    path: 'ballers-list', component: BallersListComponent
  },
  {
    path: 'baller-details', component: BallerDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
