import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { Baller } from '../../objects/Baller';

@Component({
  selector: 'app-ballers-list',
  templateUrl: './ballers-list.component.html',
  styleUrls: ['./ballers-list.component.css']
})
export class BallersListComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit(): void {
  }

  ballers: Array<Baller> = this.dataService.getBallers()

  onBallerClick(baller: Baller) {
    this.router.navigate(['baller-details'], { queryParams: { data: JSON.stringify(baller)}})
  }

}
