import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BallersListComponent } from './ballers-list.component';

describe('BallersListComponent', () => {
  let component: BallersListComponent;
  let fixture: ComponentFixture<BallersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BallersListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BallersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
