import { Component, OnInit } from '@angular/core';
import { Baller } from 'src/app/objects/Baller';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { ChartOptions } from 'chart.js';

@Component({
  selector: 'app-baller-details',
  templateUrl: './baller-details.component.html',
  styleUrls: ['./baller-details.component.css']
})
export class BallerDetailsComponent implements OnInit {

  sub: any;
  baller?: Baller;
  ballers: Baller[] = [];

  pieChartDatasets: any[] = [{
    data: []
  }]
  pieChartLegend: boolean = true
  pieChartPlugins: any = []
  pieChartOptions: ChartOptions<'pie'> = {
    responsive: false
  }
  pieChartLabels: any = []

  constructor(private dataService: DataService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.sub = this.route.queryParams.subscribe(v => this.baller = JSON.parse(v["data"]));
    this.setPieChartDataset()
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  setPieChartDataset(): void {
    this.ballers = this.dataService.getBallers()
    this.ballers.forEach(b => {
      this.pieChartLabels.push(b.name)
      this.pieChartDatasets[0].data.push(b.value)
    });
  }

}
