import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BallerDetailsComponent } from './baller-details.component';

describe('BallerDetailsComponent', () => {
  let component: BallerDetailsComponent;
  let fixture: ComponentFixture<BallerDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BallerDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BallerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
