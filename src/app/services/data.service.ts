import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Baller } from '../objects/Baller';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  ballers : Baller[] =
  [
    {
        "name": "Neymar",
        "club": "Paris SG",
        "height": 175,
        "weight": 68,
        "age": 27,
        "value": 106000000,
        "wage": 700000
    },
    {
        "name": "Kevin De Bruyne",
        "club": "Man City",
        "height": 181,
        "weight": 70,
        "age": 27,
        "value": 102000000,
        "wage": 275000
    },
    {
        "name": "Kylian Mbappé",
        "club": "Paris SG",
        "height": 178,
        "weight": 73,
        "age": 20,
        "value": 100000000,
        "wage": 450000
    },
    {
        "name": "Eden Hazard",
        "club": "R. Madrid",
        "height": 173,
        "weight": 76,
        "age": 28,
        "value": 96000000,
        "wage": 625000
    },
    {
        "name": "Robert Lewandowski",
        "club": "FC Bayern",
        "height": 184,
        "weight": 78,
        "age": 30,
        "value": 93000000,
        "wage": 375000
    },
    {
        "name": "Mohamed Salah",
        "club": "Liverpool",
        "height": 175,
        "weight": 72,
        "age": 26,
        "value": 92000000,
        "wage": 230000
    },
    {
        "name": "Sadio Mané",
        "club": "Liverpool",
        "height": 175,
        "weight": 69,
        "age": 27,
        "value": 92000000,
        "wage": 210000
    },
    {
        "name": "Sergio Agüero",
        "club": "Man City",
        "height": 172,
        "weight": 74,
        "age": 31,
        "value": 91000000,
        "wage": 275000
    },
    {
        "name": "Harry Kane",
        "club": "Tottenham",
        "height": 188,
        "weight": 86,
        "age": 25,
        "value": 89000000,
        "wage": 230000
    }
];

  getBallers(): Baller[] {
    return this.ballers;
  }

  getBaller(index: number) {
    return this.ballers[index];
  }
}
